import argparse
import subprocess
import time

def get_string():
    repo_name = subprocess.check_output(["git", "branch", "--show-current"])
    ret = 'This is a result file from ' + repo_name.decode("utf-8")[:-1] + ' branch'
    return ret
    
def generate_result(result_path):    
    generate_result_para(result_path, '')
    
def generate_result_para(result_path, parameter):    
    ir = 10
    for i in range(ir):
        print(f"Waiting ... {i+1} / {ir}" )
        time.sleep(1)
    file = open(result_path,"w")
    file.writelines( get_string() + ' ' +parameter)
    file.close()

def main(result_path, para):
    result_path = None
    if args['result_path']:
        result_path=args['result_path']
    else:
        print('No path. No file will be created')
    
    para = None
    if args['parameter']:
        para = args['parameter']
    else:
        print('No input parameter')
        
    if result_path is not None:
        if para is None:
            generate_result(result_path)
        else:
            generate_result_para(result_path, para)
    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Mock test for CICD.')
    parser.add_argument('--result_path', '-r', type=str, help='A path for creating the output file')
    parser.add_argument('--parameter', '-p', type=str, help='A parameter')
    args = vars(parser.parse_args())
    print(args)
    main(args['result_path'], args['parameter'])
  